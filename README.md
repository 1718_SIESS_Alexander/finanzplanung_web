# Finanzplanung_Web

##Über dieses Repository
Diese Repository dient zur Versionsverwaltung, des Finanzplanungs-Webapplikation.

##Allgemeine Informationen zu dem Projekt
### Ausgangslage:
Die momentane Online-Banking Plattform von der Bank Raiffeisen, namens ELBA, mangelt an Funktionen, mit denen man seine Ausgaben & Eingaben verwalten kann. Man kann schon vorher festgelegte Kategorien vergeben oder die Buchungssätze mit einem sog. „Tag“ zu Wiedererkennung bestücken.
Man kann von ELBA die Buchungssätze alle in einem CSV-Formt downloaden.
Ein solcher Datensatz kann beispielsweise wie folgt aussehen:
02.10.2017;MERKUR DANKT 3356  K2   30.09. 12:02; 30.09.2017; -141,71;EUR;30.09.2017 12:03:28:000

### Idee:
Da man praktisch alle Buchungssätze legal bekommt, entwickeln wir ein kleine Web-Applikation rein zum Verwalten der Finanzen. Hierbei ist dazu gesagt, dass alles rein theoretisch ist und mit diesem Tool, Geld, in keinster Weise bewegt werden kann. 

### Hintergedanken:
Da ich selbst eine recht ungewöhnliche Finanzsituation habe und somit mehr Verwaltungsmöglichkeiten benötige, möchte ich dieses Tool bei Erfolg in Zukunft weiter verwenden. Die Idee ist, die Applikation zu Hause auf einem Raspberry laufen zu lassen, auf die man dann im Heimnetzwerk von allen Geräten aus zugreinfen kann.

### Umsetzung:
Die Applikation soll mithilfe des Java Frameworks „Spring“ umgesetzt werden. Wenn Zeit bleibt, wird die Infrastruktur mittes des Raspberrys auch noch umgesetzt.

##Whom to ask?
Alexander Siess

Tobias Kerbl