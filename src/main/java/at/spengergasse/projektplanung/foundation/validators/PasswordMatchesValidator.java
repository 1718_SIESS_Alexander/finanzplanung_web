package at.spengergasse.projektplanung.foundation.validators;

import at.spengergasse.projektplanung.domain.UserCreateDTO;
import at.spengergasse.projektplanung.domain.UserUpdateDTO;
import at.spengergasse.projektplanung.foundation.annotations.PasswordMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){

        if(obj instanceof UserCreateDTO) {
            UserCreateDTO user = (UserCreateDTO) obj;
            return user.getPassword().equals(user.getMatchingPassword());
        }
        if(obj instanceof UserUpdateDTO) {
            UserUpdateDTO user = (UserUpdateDTO) obj;
            //there do not have to be passwords set
            if(passwordsShouldNotBeChanged(user)) {
                return true;
            }
            return user.getPassword().equals(user.getMatchingPassword());
        }
        return false;
    }

    public boolean passwordsShouldNotBeChanged(UserUpdateDTO user)
    {
        if(user.getPassword().equals("") && user.getMatchingPassword().equals("")) {
            return true;
        }
        return false;
    }
}
