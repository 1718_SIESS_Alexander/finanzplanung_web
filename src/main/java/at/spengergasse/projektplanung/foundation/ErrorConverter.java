package at.spengergasse.projektplanung.foundation;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

@Component
public class ErrorConverter {

    public List<String> convertErrors(BindingResult result) {
        List<String> allErrors = new ArrayList<>();

        String field;
        String message;

        List<FieldError> fieldErrors = result.getFieldErrors();
        for (int i = 0; i < fieldErrors.size(); i++)
        {
            field = fieldErrors.get(i).getField();
            message = fieldErrors.get(i).getDefaultMessage();

            if(message == null){
                message = "already exists!";
            }
            if(message.equals("darf nicht leer sein")) {
                message = "must not be empty!";
            }
            allErrors.add(field+" "+message);
        }
        //Object Errors -> to get the password don't match error
        List<ObjectError> objectErrors = result.getAllErrors();
        for (int i = 0; i < objectErrors.size(); i++)
        {
            field = objectErrors.get(i).getObjectName();
            message = objectErrors.get(i).getDefaultMessage();
            if(message != null && message.equals("Passwords don't match")){
                allErrors.add(field+" "+message);
            }
        }
        return allErrors;
    }
}
