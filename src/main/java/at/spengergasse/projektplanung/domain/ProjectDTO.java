package at.spengergasse.projektplanung.domain;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class ProjectDTO {

    @NotNull
    @NotEmpty
    private String projectname;

    @NotNull
    @NotEmpty
    private String type;

    @NotNull
    @NotEmpty
    private String department;

    @NotNull
    @NotEmpty
    private String description;
}
