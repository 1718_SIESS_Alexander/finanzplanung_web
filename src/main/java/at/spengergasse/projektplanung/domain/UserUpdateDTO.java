package at.spengergasse.projektplanung.domain;

import at.spengergasse.projektplanung.foundation.annotations.PasswordMatches;
import at.spengergasse.projektplanung.foundation.annotations.ValidEmail;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@PasswordMatches
public class UserUpdateDTO {

    @NotNull
    @NotEmpty
    private String username;

    @NotNull
    @NotEmpty
    private String firstname;

    @NotNull
    @NotEmpty
    private String lastname;

    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;

    private String dateofbirth;

    private String password;
    private String matchingPassword;
}
