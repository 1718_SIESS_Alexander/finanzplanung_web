package at.spengergasse.projektplanung.domain;

import lombok.*;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;

@Entity
@Data
@Builder
public class Privilege extends AbstractPersistable<Long> {

    @Column(nullable = false, unique = true)
    private String name;

}