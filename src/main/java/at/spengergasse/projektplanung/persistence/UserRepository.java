package at.spengergasse.projektplanung.persistence;

import at.spengergasse.projektplanung.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Repository;

@Repository
@EnableJpaAuditing
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
    int removeUserByUsername(String username);
}
