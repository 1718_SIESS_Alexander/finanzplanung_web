package at.spengergasse.projektplanung.persistence;

import at.spengergasse.projektplanung.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.stereotype.Repository;

@Repository
@EnableJpaAuditing
public interface ProjectRepository extends JpaRepository<Project, Long> {

    Project findByProjectname(String projectname);
}
