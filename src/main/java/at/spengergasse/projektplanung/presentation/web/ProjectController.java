package at.spengergasse.projektplanung.presentation.web;

import at.spengergasse.projektplanung.domain.Project;
import at.spengergasse.projektplanung.domain.ProjectDTO;
import at.spengergasse.projektplanung.foundation.ErrorConverter;
import at.spengergasse.projektplanung.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/home")
    public String home(Model model, @RequestParam("page") Optional<Integer> page,
                                    @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Project> projectPage = projectService.findProjectsPaginated(PageRequest.of(currentPage - 1, pageSize));

        model.addAttribute("projectPage", projectPage);

        int totalPages = projectPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                                            .boxed()
                                            .collect(Collectors.toList());
            Collections.sort(pageNumbers);
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "home";
    }

    @PostMapping("/home/createProject")
    public ModelAndView createNewProject(@ModelAttribute("project") @Valid ProjectDTO projectDTO,
                                         BindingResult result, WebRequest request, Errors errors){
        Project project = null;
        if(!result.hasErrors()) {
            project = createProject(projectDTO);
        }
        if(project == null) {
            result.rejectValue("projectname", "message.regError");
        }
        if(result.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("homeError", "project", project);
            modelAndView.addObject("errors", new ErrorConverter().convertErrors(result));
            return modelAndView;
        }
        else {
            return new ModelAndView("redirect:/home", "project", projectDTO);
        }
    }

    public Project createProject(ProjectDTO projectDTO){

        Project project = null;
        try {
            project = projectService.createNewProject(projectDTO);
        }
        catch(IllegalArgumentException exc) {
            return null;
        }
        return project;
    }

    @PostMapping("/home/editProject/{projectId}")
    public ModelAndView editNewProject(@ModelAttribute("project") @Valid ProjectDTO projectDTO,
                                       BindingResult result, WebRequest request, Errors errors, @PathVariable long projectId) {
        Project updated = new Project();
        if (!result.hasErrors()) {
            updated = updateProject(projectDTO, result, projectId);
        }
        if (updated == null) {
            result.rejectValue("username", "message.regError");
        }
        if (result.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("homeError", "project", updated);
            modelAndView.addObject("errors", new ErrorConverter().convertErrors(result));
            return modelAndView;
        }
        else {
            return new ModelAndView("redirect:/home", "user", projectDTO);
        }
    }

    private Project updateProject(ProjectDTO updateDto, BindingResult result, long projectId) {
        Project updated = null;
        try {
            updated = projectService.updateNewProject(updateDto, projectId);

        } catch (IllegalArgumentException e) {
            return null;
        }
        return updated;
    }

    @PostMapping("/home/deleteProject/{projectId}")
    public ModelAndView deleteCurrentProject(@PathVariable long projectId){
        projectService.deleteProject(projectId);
        return new ModelAndView("redirect:/home");
    }
}
