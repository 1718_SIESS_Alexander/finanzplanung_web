package at.spengergasse.projektplanung.presentation.web;

import at.spengergasse.projektplanung.domain.User;
import at.spengergasse.projektplanung.domain.UserUpdateDTO;
import at.spengergasse.projektplanung.foundation.ErrorConverter;
import at.spengergasse.projektplanung.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ProfileController {

    @Autowired
    private UserService userService;

    @GetMapping("/profile")
    public String getProfilePage(Model model) {
        model.addAttribute("userDetails",userService.getPrincipal());
        return "profile.html";
    }

    @PostMapping(value = "/profile")
    public ModelAndView updateUserAccount (@ModelAttribute("user") @Valid UserUpdateDTO updateDto,
                                           BindingResult result, WebRequest request, Errors errors) {
        User updated = new User();
        if (!result.hasErrors()) {
            updated = updateUserAccount(updateDto, result);
        }
        if (updated == null) {
            result.rejectValue("username", "message.regError");
        }
        if (result.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("profile", "user", updateDto);
            modelAndView.addObject("userDetails",userService.getPrincipal());//otherwise, there would be no values in the profile form after reloading
            modelAndView.addObject("errors", new ErrorConverter().convertErrors(result));
            return modelAndView;
        }
        else {
            return new ModelAndView("redirect:/login", "user", updateDto);
        }
    }

    private User updateUserAccount(UserUpdateDTO updateDto, BindingResult result) {
        User updated = null;
        try {
            updated = userService.updateCurrentUser(updateDto);

        } catch (IllegalArgumentException e) {
            return null;
        }
        return updated;
    }
}
