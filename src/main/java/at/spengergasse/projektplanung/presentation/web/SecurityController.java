package at.spengergasse.projektplanung.presentation.web;

import at.spengergasse.projektplanung.domain.User;
import at.spengergasse.projektplanung.domain.UserCreateDTO;
import at.spengergasse.projektplanung.foundation.ErrorConverter;
import at.spengergasse.projektplanung.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class SecurityController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/registration")
    public String registration(WebRequest request, Model model) {

        UserCreateDTO userCreateDto = new UserCreateDTO();
        model.addAttribute("user", userCreateDto);
        return "registration";
    }

    @PostMapping(value = "/registration")
    public ModelAndView registerUserAccount (@ModelAttribute("user") @Valid UserCreateDTO accountDto,
                                             BindingResult result, WebRequest request, Errors errors) {
        User registered = new User();
        if (!result.hasErrors()) {
            registered = createUserAccount(accountDto, result);
        }
        if (registered == null) {
            result.rejectValue("username", "message.regError");
        }
        if (result.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("registration", "user", accountDto);
            modelAndView.addObject("errors", new ErrorConverter().convertErrors(result));
            return modelAndView;
        }
        else {
            return new ModelAndView("redirect:/home", "user", accountDto);
        }
    }

    private User createUserAccount(UserCreateDTO accountDto, BindingResult result) {
        User registered = null;
        try {
            registered = userService.registerNewUserAccount(accountDto);
        } catch (IllegalArgumentException e) {
            return null;
        }
        return registered;
    }
}
