package at.spengergasse.projektplanung.presentation.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FunctionController {

    @GetMapping("/document_lines")
    public String getDocumentLines() {
        return "document_lines.html";
    }

    @GetMapping("/code_lines")
    public String getCodeLines() {
        return "code_lines.html";
    }

    @GetMapping("/eva")
    public String getEVA() {
        return "eva.html";
    }

    @GetMapping("/burndown")
    public String getBurndown() {
        return "burndown.html";
    }
}
