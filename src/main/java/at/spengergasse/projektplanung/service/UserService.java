package at.spengergasse.projektplanung.service;

import at.spengergasse.projektplanung.domain.Privilege;
import at.spengergasse.projektplanung.domain.User;
import at.spengergasse.projektplanung.domain.UserCreateDTO;
import at.spengergasse.projektplanung.domain.UserUpdateDTO;
import at.spengergasse.projektplanung.persistence.PrivilegeRepository;
import at.spengergasse.projektplanung.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Transactional
    public User registerNewUserAccount(UserCreateDTO accountDto) throws IllegalArgumentException {

        if (usernameExist(accountDto.getUsername())) {
            throw new IllegalArgumentException(
                    "There is already an account with that username: "+accountDto.getUsername());
        }
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        Privilege privilege = privilegeRepository.findByName("ROLE_USER");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
        LocalDate date = LocalDate.parse(accountDto.getDateofbirth(), formatter);

        User user = User.builder()
                .username(accountDto.getUsername())
                .firstname(accountDto.getFirstname())
                .lastname(accountDto.getLastname())
                .email(accountDto.getEmail())
                .dateofbirth(date)
                .password(passwordEncoder.encode(accountDto.getPassword()))
                .privileges(new HashSet<Privilege>(Arrays.asList(privilege)))
                .build();

        return repository.save(user);
    }

    @Transactional
    public User updateCurrentUser(UserUpdateDTO accountDto)  {

        User updated = getPrincipal();

        if(!accountDto.getUsername().equals(updated.getUsername())) {
            updated.setUsername(accountDto.getUsername());
        }
        if(!accountDto.getFirstname().equals(updated.getFirstname())){
            updated.setFirstname(accountDto.getFirstname());
        }
        if(!accountDto.getLastname().equals(updated.getLastname())){
            updated.setLastname(accountDto.getLastname());
        }
        if(!accountDto.getEmail().equals(updated.getEmail())){
            updated.setEmail(accountDto.getEmail());
        }
        if(!accountDto.getDateofbirth().equals(updated.getDateofbirth().toString())){
            //otherwise, there will be a parsing exception
            if(accountDto.getDateofbirth().equals("")) {
                updated.setDateofbirth(null);
            }
            else {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
                LocalDate date = LocalDate.parse(accountDto.getDateofbirth(), formatter);
                updated.setDateofbirth(date);
            }
        }
        if(!accountDto.getPassword().equals("") && !accountDto.getPassword().equals(updated.getPassword())) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            updated.setPassword(passwordEncoder.encode(accountDto.getPassword()));
        }
        return repository.save(updated);
    }

    private boolean usernameExist(String username) {
        User user = repository.findByUsername(username);
        if(user != null) {
            return true;
        }
            return false;
    }

    public User getPrincipal()
    {
        User resultUser;
        String username;

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            username = authentication.getName();

            resultUser = repository.findByUsername(username);
            return resultUser;
        }
        return null;
    }
}
