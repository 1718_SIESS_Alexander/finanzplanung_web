package at.spengergasse.projektplanung.service;

import at.spengergasse.projektplanung.domain.Project;
import at.spengergasse.projektplanung.domain.ProjectDTO;
import at.spengergasse.projektplanung.persistence.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    private List<Project> projects;

    @Transactional
    public Project createNewProject(ProjectDTO projectDTO)
    {
        if(projectExits(projectDTO.getProjectname())){
            throw new IllegalArgumentException("project already exists!");
        }
        Project project = Project.builder()
                            .projectname(projectDTO.getProjectname())
                            .type(projectDTO.getType())
                            .department(projectDTO.getDepartment())
                            .description(projectDTO.getDescription())
                            .build();

        return projectRepository.save(project);
    }

    @Transactional
    public Project updateNewProject(ProjectDTO projectDTO, long projectId){
        Project updated = projectRepository.findById(projectId).get();

        if(!projectDTO.getProjectname().equals(updated.getProjectname())) {
            updated.setProjectname(projectDTO.getProjectname());
        }
        if(!projectDTO.getType().equals(updated.getType())){
            updated.setType(projectDTO.getType());
        }
        if(!projectDTO.getDepartment().equals(updated.getDepartment())){
            updated.setDepartment(projectDTO.getDepartment());
        }
        if(!projectDTO.getDescription().equals(updated.getDescription())){
            updated.setDescription(projectDTO.getDescription());
        }

        return projectRepository.save(updated);
    }

    private boolean projectExits(String projectname) {
        Project project = projectRepository.findByProjectname(projectname);
        if(project != null){
            return true;
        }
        return false;
    }

    public List<Project> findAllProjects()
    {
        return projectRepository.findAll();
    }

    @Transactional
    public void deleteProject(Long projectID){
        projectRepository.deleteById(projectID);
    }

    public Page<Project> findProjectsPaginated(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;

        projects = findAllProjects();
        List<Project> list;

        if (projects.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, projects.size());
            list = projects.subList(startItem, toIndex);
        }

        Page<Project> projectPage
                = new PageImpl<Project>(list, PageRequest.of(currentPage, pageSize), projects.size());

        return projectPage;
    }
}
