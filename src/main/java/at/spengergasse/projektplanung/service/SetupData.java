package at.spengergasse.projektplanung.service;

import at.spengergasse.projektplanung.domain.Privilege;
import at.spengergasse.projektplanung.domain.Project;
import at.spengergasse.projektplanung.domain.User;
import at.spengergasse.projektplanung.persistence.PrivilegeRepository;
import at.spengergasse.projektplanung.persistence.ProjectRepository;
import at.spengergasse.projektplanung.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.*;

@Component
public class SetupData {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @PostConstruct
    public void init() {
        initPrivileges();
        initUsers();
        initProjects();
    }

    private void initPrivileges() {

        Privilege privilege1 = Privilege.builder()
                .name("ROLE_ADMIN")
                .build();
        privilegeRepository.save(privilege1);

        Privilege privilege2 = Privilege.builder()
                .name("ROLE_USER")
                .build();
        privilegeRepository.save(privilege2);
    }

    private void initUsers() {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        Privilege privilege1 = privilegeRepository.findByName("ROLE_ADMIN");
        Privilege privilege2 = privilegeRepository.findByName("ROLE_USER");

        User user1 = User.builder()
                .username("kerbl")
                .firstname("Tobias")
                .lastname("Kerbl")
                .email("ker16978@spengergasse.at")
                .dateofbirth(LocalDate.of(2000,3,25))
                .password(passwordEncoder.encode("kerbl123"))
                .privileges(new HashSet<Privilege>(Arrays.asList(privilege1)))
                .build();
        userRepository.save(user1);

        User user2 = User.builder()
                .username("siess")
                .firstname("Alexander")
                .lastname("Siess")
                .email("sie17055@spengergasse.at")
                .dateofbirth(LocalDate.of(2000,3,25))
                .password(passwordEncoder.encode("siess123"))
                .privileges(new HashSet<Privilege>(Arrays.asList(privilege2)))
                .build();
        userRepository.save(user2);
    }

    private void initProjects() {

        User user1 = userRepository.findByUsername("kerbl");

        Project project1 = Project.builder()
                            .projectname("Diplomprojekt")
                            .type("IT")
                            .department("Höhere Anstalt für Informatik")
                            .description("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor" +
                                         "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam" +
                                         "et justo duo dolor")
                            .build();
        projectRepository.save(project1);

        Project project2 = Project.builder()
                .projectname("NVS Projekt")
                .type("NVS")
                .department("IT")
                .description("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor" +
                        "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam" +
                        "et justo duo dolor")
                .user(user1)
                .build();
        projectRepository.save(project2);

        Project project3 = Project.builder()
                .projectname("POS Thymeleaf Projekt")
                .type("POS")
                .department("IT")
                .description("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor" +
                        "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam" +
                        "et justo duo dolor")
                .user(user1)
                .build();
        projectRepository.save(project3);

        Project project4 = Project.builder()
                .projectname("BWM Businessplan")
                .type("BWM")
                .department("Geschäftsführung")
                .description("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor" +
                        "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam" +
                        "et justo duo dolor")
                .user(user1)
                .build();
        projectRepository.save(project4);

        Project project5 = Project.builder()
                .projectname("DBI Layered Architecture Project")
                .type("DBI")
                .department("IT")
                .description("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor" +
                        "invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam" +
                        "et justo duo dolor")
                .user(user1)
                .build();
        projectRepository.save(project5);

        Project project6 = Project.builder()
                .projectname("POS Angular APP MOTSCHUL Projekt")
                .type("POS")
                .department("IT")
                .description("AAAAAAAH APP ÄÄÄÄÄÄÄÄÄHHHHHMMMMM MOTSCHUL, AAAAAAH Das ist sehr ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄHHHHHMMMMM dünn!")
                .user(user1)
                .build();
        projectRepository.save(project6);


    }
}
