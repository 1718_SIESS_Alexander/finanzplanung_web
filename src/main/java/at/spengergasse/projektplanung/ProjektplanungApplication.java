package at.spengergasse.projektplanung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjektplanungApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjektplanungApplication.class, args);
    }
}
