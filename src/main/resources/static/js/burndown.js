Highcharts.chart('container', {

    title: {
        text: 'Burn-Down-Chart'
    },
    chart: {
        style: {
            fontFamily: "Segoe UI"
        }
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Number of tasks'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    tooltip: {
        valueSuffix: ' tasks',
        crosshairs: true,
        shared: true
    },
    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: '01/09/2018'
        }
    },

    data: {
        table: document.getElementById('datatable')
    },

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    },

    series : [{ id: 'series1' }, { id: 'series2' }]

});