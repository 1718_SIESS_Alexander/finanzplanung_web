Highcharts.chart('container', {

    title: {
        text: 'Documentation Lines'
    },
    chart: {
        style: {
            fontFamily: "Segoe UI"
        }
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Number of Lines'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    tooltip: {
        valueSuffix: ' lines',
        crosshairs: true,
        shared: true
    },
    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: '09/29/2018'
        }
    },

    data: {
        table: document.getElementById('datatable')
    },

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    },

    series : [{ id: 'series1' }, { id: 'series2' }]

});