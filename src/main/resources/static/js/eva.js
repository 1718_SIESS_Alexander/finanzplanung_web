$( document ).on("click", "#datatable tr", function() {
    if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
    } else {
        var a = $(this).parent();
        if (a[0].tagName.toLowerCase() != "thead"){
            $(this).addClass('selected').siblings().removeClass('selected');
        }
    }
});
$( document ).on("dblclick", "#datatable td", function() {
    var OriginalContent = $(this).text();
    $(this).addClass("cellEditing");
    $(this).html("<input type='text' value='" +"' />");
    $(this).children().first().focus();
    $(this).children().first().keypress(function (e) {
        if (e.which == 13) {
            var newContent = $(this).val();
            $(this).parent().text(newContent);
            $(this).parent().removeClass("cellEditing");
        }
    });
    $(this).children().first().blur(function(){
        $(this).parent().text(OriginalContent);
        $(this).parent().removeClass("cellEditing");
    });
});
$( document ).on("click", "#refresh", function() {

    var complete = function(options) {
        // get the two HC-series objects
        var chart = $('#container').highcharts();
        var series1 = chart.get('series1');
        var series2 = chart.get('series2');
        var series3 = chart.get('series3');

        // set new data
        series1.setData(options.series[0].data, false);
        series2.setData(options.series[1].data, false);
        series3.setData(options.series[2].data, false);

        // and redraw chart
        chart.redraw();
    };

    // call the data parser of HC
    Highcharts.data({
        table : document.getElementById('datatable'),
        complete : complete
    });
});
$( document ).on("click", "#addrow", function() {

    var table = document.getElementById("datatable");
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    cell1.innerHTML = "01/01/2099";
    cell2.innerHTML = "0";
    cell3.innerHTML = "0";
    cell4.innerHTML = "0";
});
$( document ).on("click", "#deleterow", function() {
    $('.selected').remove();
});